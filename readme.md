# indigo-fiber

Fiber is a lightweight, bare-bones framework fro building reactive JavaScirpt apps. Using fiber is pretty different to using other frameworks; there's no dependencies you need to install or runtime environments to work in - using it just means structuring your app's implementation around a simple construct: the variable fiber.

A variable fiber is a special kind of variable which can be monitored for value changes. It's a basic unit of reactivity that can built upon to implement fully reactive UIs that stay in sync with data sources. While a number of "plugins" have been written to provide supplementary features, you certainly don't need them to build an app with fiber.


## Variable Fiber API (Draft)

Variable fibers can be implemented in a number of ways and we don't wish to impose one particular implementation. Instead, we've only defined an API which compliant implementations should follow. Any software which interops with this API will hence be able to interop with any particular fiber implementation.


### 1. Variable Fibers

A variable fiber is a **function** `f` which takes a value `v`. `v` can be one of three types:

* (1) Callable - a JavaScript object which has a invokable `call()` method.
* (2) Undefined - the JavaScript primitive `undefined`
* (3) Literal - anything other than the first two types.


### 2. Treatment of `v`

* If `v` is of type 1 (callable), then it **MUST** be treated as a listener. Listeners are invoked whenever the variable fiber's value changes (see section 3).

* If `v` is of type 2 (undefined), then the variable fiber **MUST** return its current value. A varible fiber's initial **SHOULD** be determined by a varible fiber constructor (see section 4).

* If `v` is of type 3 (literal), then the variable fiber **MUST** set its current value to `v`.


### 3. Invoking Listeners

Listeners are functions which are called whenever a variable fiber that they're subscribed to changes. Any object with an invokable `call()` method can be used as a listener.

A variable fiber's value **SHOULD ONLY** be changed via passing in a literal as described in section 2. When this occurs, the fiber **MUST** invoke any listener subscribed to it, passing in the new value as an argument.

Listeners **MUST** also be called upon first subscribing to a fiber with the fiber's current value given as an argument.


### 4. Variable Constructors

A variable fiber constructor is a function which takes in a value `x` and one or more listeners. A new variable fiber should be created when the constructor is called. The fiber's value **MUST** be set to `x` so long as `x` is of type 2 (undefined) or type 3 (literal). Every listener **MUST** then be subscribed to the fiber. The same subscription behaviour applies here as in section 3. Finally, the newly created fiber should be returned from the constructor.


This spec is still an early draft and may be subject to significant change. However, the basic objects discussed will not be removed.


## Variable Fiber Implementation

A simple implementation of the fiber API can be written in one line:

```js
// fiber.js

export default (value, ...use) => to =>
	to.call
		? use.push(to) + to(value)
		: use.forEach(use => use(value=to))
```

The exported function is a variable fiber constructor and can be used like so:

1. Import the library

```js
import fiber from './fiber.js'
```

2. Create a new fiber variable. Its initial value is set to `'Alice'`.

```js
const name = fiber('Alice')
```

3. Register an event listener. In this case, we've registered `console.log()`, so any changes to `name` will be output to the console. Note that the listener is run once when first registered.

```js
name(console.log)
```

```js
'Alice'
```

```js
name('Sam')
```

```js
'Sam'
```

## Adding Multiple Listeners

You can register any number of listeners to a fiber variable. For example:

```js
function showAge(age) {
	console.log(`Age is currently ${age}`)
}

function showYoung(age) {
	if (age < 20) {
		console.log('You are young!')
	}
}

const myAge = fiber(19, showAge, showYoung)
```

```js
'Age is currently 19'
'You are young'
```

```js
myAge(30)
```

```js
'Age is currently 30'
```

## Application: Reactive Element Properties

Add this function to your project:

```js
function bind(element, fibers) {

	Object.entries(fibers).forEach(([key, fiber]) => {
		fiber(value => element[key] = value)
	})
}
```

Now you can make any element's properties reactive.

```js
const div = document.createElement('div')
const id = fiber('neutral')

bind(div, { id })
```

```js
console.log(div)
```

```html
<div id='neutral'></div>
```

```js
id('danger')

console.log(div)
```

```html
<div id='danger'></div>
```

## Application: Reactive Components

Taking this idea further, we can simplify the construction of components.

```js
function App({ background, ...rest }) {

	const root = document.createElement('div')
	const image = document.createElement('img')

	root.append(image)

	bind(root, rest)
	bind(image, { src: background })

	return root
}
```

```js
const background = fiber('./cat.jpg')
const id = fiber('my-app')

const app = App({
	id,
	background
})
```

```js
console.log(app)
```

```html
<div id='my-app'>
    <img src='./cat.jpg'>
</div>
```

```js
background('./dog.jpg')

console.log(app)
```

```html
<div id='my-app'>
    <img src='./dog.jpg'>
</div>
```

It's also possible to achieve *declarative* components using a similar system; in other words, we can do something very similar to React or Vue using nothing more than `fiber()` and `bind()`. This has been implemented by [indigo-fiber-declarative-elements](https://codeberg.org/alice/indigo-fiber-declarative-elements).
