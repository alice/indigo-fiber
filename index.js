export default (value, ...use) => to =>
    to.call
        ? use.push(to) + to(value)
        : use.forEach(use => use(value = to))
